package org.smd.entity;

import javax.persistence.*;

@Entity
@Table(name = "my_token")
public class MyToken {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "terminate")
    private Boolean terminate;

    public MyToken() {

    }

    public MyToken(String id) {
        this.id = id;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Boolean getTerminate() {
        return terminate;
    }

    public void setTerminate(Boolean terminate) {
        this.terminate = terminate;
    }
}
