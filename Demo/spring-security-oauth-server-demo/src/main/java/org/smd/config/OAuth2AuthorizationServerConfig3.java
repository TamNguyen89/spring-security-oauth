package org.smd.config;

import org.smd.entity.MyToken;
import org.smd.repository.MyTokenRepository;
import org.smd.service.CustomUserDetail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
import org.springframework.security.oauth2.provider.token.store.KeyStoreKeyFactory;

import javax.sql.DataSource;

@Configuration
@EnableAuthorizationServer
@PropertySource({ "classpath:persistence.properties" })
public class OAuth2AuthorizationServerConfig3 extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private Environment env;

    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;

    @Autowired
    private MyTokenRepository myTokenRepository;

    //

    @Override
    public void configure(final AuthorizationServerSecurityConfigurer oauthServer) throws Exception {
        oauthServer.tokenKeyAccess("permitAll()").checkTokenAccess("isAuthenticated()");
    }

    @Override
    public void configure(final ClientDetailsServiceConfigurer clients) throws Exception { // @formatter:off
//		clients.inMemory().withClient("fooClientIdPassword").secret("secret")
//				/* 1 */.authorizedGrantTypes("password", "authorization_code", "refresh_token")
//				/* 2 */.scopes("foo", "read", "write").accessTokenValiditySeconds(120) // 30
//																						// seconds,
//																						// so
//																						// that
//																						// it
//																						// expires
//																						// quickly
//				/* 3 */.refreshTokenValiditySeconds(2592000) // 30 days
//		;
        clients.jdbc(dataSource());
	} // @formatter:on

    @Override
    public void configure(final AuthorizationServerEndpointsConfigurer conf) { // @formatter:off
		conf.tokenStore(tokenStore())
                .allowedTokenEndpointRequestMethods(HttpMethod.GET, HttpMethod.POST)
				.accessTokenConverter(accessTokenConverter())
                .tokenServices(tokenServices())
                .authenticationManager(authenticationManager);
	} // @formatter:on

    // JWT

    @Bean
    @Primary
    public DefaultTokenServices tokenServices() {
        DefaultTokenServices tokenServices = new CustomTokenService(myTokenRepository);
        tokenServices.setTokenStore(tokenStore());
        /* 4 */tokenServices.setSupportRefreshToken(true);
        tokenServices.setTokenEnhancer(accessTokenConverter());
        return tokenServices;
    }

    @Bean
    public TokenStore tokenStore() {
        return new JwtTokenStore(accessTokenConverter());
    }

    @Bean
    public JwtAccessTokenConverter accessTokenConverter() {
        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
        KeyStoreKeyFactory keyStoreKeyFactory =
                new KeyStoreKeyFactory(new ClassPathResource("mytest.jks"), "mypass".toCharArray());
        converter.setKeyPair(keyStoreKeyFactory.getKeyPair("mytest"));
        return converter;
    }


    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("jdbc.driverClassName"));
        dataSource.setUrl(env.getProperty("jdbc.url"));
        dataSource.setUsername(env.getProperty("jdbc.user"));
        dataSource.setPassword(env.getProperty("jdbc.pass"));
        return dataSource;
    }

    static class CustomTokenService extends DefaultTokenServices {

        private MyTokenRepository myTokenRepository;

        public CustomTokenService(MyTokenRepository myTokenRepository) {
            this.myTokenRepository = myTokenRepository;
        }

        @Override
        public OAuth2AccessToken createAccessToken(OAuth2Authentication authentication) throws AuthenticationException {
            OAuth2AccessToken oAuth2AccessToken = super.createAccessToken(authentication);
            String jti = (String) oAuth2AccessToken.getAdditionalInformation().get("jti");
            MyToken myToken = new MyToken(jti);
            myToken.setTerminate(false);
            myTokenRepository.save(myToken);
            return oAuth2AccessToken;
        }






    }

}
