package org.smd.repository;

import org.smd.entity.MyToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by tamnguyen on 03/10/2017.
 */
@Repository
public interface MyTokenRepository extends JpaRepository<MyToken, String> {
}
